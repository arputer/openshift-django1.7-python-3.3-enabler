from setuptools import setup
import os

required_packages = [
    'Django==1.7',
    'django-extensions',
    'djangorestframework',
    'jsonfield',
    'pymysql',
    'Pillow',
    'six',
    'django-suit',
    'psycopg2',
    'pycrypto',
]

setup(name='YourAppName',
      version='1.0',
      description='OpenShift App',
      author='Your Name',
      author_email='example@example.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=required_packages,
      )
